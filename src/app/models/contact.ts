export interface Contact{
    idNumber: String;
    fullName: String;
    direction: String;
    cellPhone: String;
    birthdayDate: String;   
 
}