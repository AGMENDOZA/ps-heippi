import { BirthdayComponent } from './components/dashboard/birthday/birthday.component';
import { TotalcontactsComponent } from './components/dashboard/totalcontacts//totalcontacts.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'totalcontacts', component: TotalcontactsComponent },
  { path: 'birthday', component: BirthdayComponent },
  { path: '', pathMatch: 'full', redirectTo: '/home' },//redirecciona cuando inicie el app
  { path: '**', pathMatch: 'full', redirectTo: '/contact' },//redirecciona al otro modulo
 // { path: '**', pathMatch: 'full', redirectTo: '/add-contact' }//redirecciona al otro modulo

];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
