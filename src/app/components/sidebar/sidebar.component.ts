import { Component, OnInit } from '@angular/core';
import M from 'materialize-css';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
   

      var elems = document.querySelectorAll('.sidenav');
      var instances = M.Sidenav.init(elems, {preventScrolling:true});
      var elems = document.querySelectorAll('.collapsible');
      var instances = M.Collapsible.init(elems);

  
    
  }

}
