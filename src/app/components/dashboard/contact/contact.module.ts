import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import { ContactComponent } from './contact.component';
import { AddContactComponent } from './add-contact/add-contact.component';
import { ListContactComponent } from './list-contact/list-contact.component';
import { DeleteContactComponent } from './delete-contact/delete-contact.component';
import { EditContactComponent } from './edit-contact/edit-contact.component';


@NgModule({
  declarations: [
    ContactComponent, 
    AddContactComponent, 
    ListContactComponent, 
    DeleteContactComponent, 
    EditContactComponent],
  imports: [
    CommonModule,
    ContactRoutingModule,
    FormsModule
  ],
 
})
export class ContactModule { }
