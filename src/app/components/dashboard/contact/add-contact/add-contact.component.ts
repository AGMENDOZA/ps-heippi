
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
//models
import { Contact } from '../../../../models/contact';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.css']
})
export class AddContactComponent implements OnInit {



  idNumber: String;
  fullName: String;
  direction: String;
  cellPhone: String;
  birthdayDate: String;
  @Output() contactAdded = new EventEmitter<Contact>();

  constructor(

  ) { }

  ngOnInit(): void {

  }
  addContact() {
    


    this.contactAdded.emit({
      idNumber: this.idNumber,
      fullName: this.fullName,
      direction: this.direction,
      cellPhone: this.cellPhone,
      birthdayDate: this.birthdayDate

    });
      this.idNumber='',
      this.fullName='',
      this.direction='',
      this.cellPhone='',
      this.birthdayDate=''

  }

  
}
