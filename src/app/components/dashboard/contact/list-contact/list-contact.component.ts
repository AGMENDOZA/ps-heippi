import { Component, OnInit, Input } from '@angular/core';
//module
import { Contact } from './../../../../models/contact';
//service
import { DataService } from './../../../../services/data.service';


import M from 'materialize-css';


@Component({
  selector: 'app-list-contact',
  templateUrl: './list-contact.component.html',
  styleUrls: ['./list-contact.component.css']
})
export class ListContactComponent implements OnInit {

  @Input('contact') contact:Contact;

  constructor(
    public dataService:DataService
  ) { }

  ngOnInit(
    
  ): void {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);
  }

  deleteContact(contact:Contact){
    const response=confirm('Are you sure delete it');
    if(response){
        this.dataService.deleteContact(contact);
    }
  }

  

}
