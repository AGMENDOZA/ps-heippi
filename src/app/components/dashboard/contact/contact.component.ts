import { DataService } from './../../../services/data.service';
import { Contact } from './../../../models/contact';
import { Component, OnInit } from '@angular/core';


import M from 'materialize-css';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contacts:Contact[];

  constructor(
    public dataService:DataService
    
  ) { }

  ngOnInit(): void {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems);

    this.contacts=this.dataService.getContact();

  }

  addContact(contact:Contact) {
 
    this.dataService.addContact(contact);
  }

 
}
