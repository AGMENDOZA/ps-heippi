import { Contact } from './../../../../models/contact';

import { Component, OnInit, Output,EventEmitter  } from '@angular/core';

//models


@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent implements OnInit {

  idNumber: String;
  fullName: String;
  direction: String;
  cellPhone: String;
  birthdayDate: String;
  @Output() contactAdded = new EventEmitter<Contact>();

  constructor(
  ) { }

  ngOnInit(): void {
  }
 
  editContact() {
    


    this.contactAdded.emit({
      idNumber: this.idNumber,
      fullName: this.fullName,
      direction: this.direction,
      cellPhone: this.cellPhone,
      birthdayDate: this.birthdayDate

    });
      this.idNumber='',
      this.fullName='',
      this.direction='',
      this.cellPhone='',
      this.birthdayDate=''

  }

  
}
