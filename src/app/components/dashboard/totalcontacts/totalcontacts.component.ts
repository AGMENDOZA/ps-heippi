import { Contact } from './../../../models/contact';
import { DataService } from './../../../services/data.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-totalcontacts',
  templateUrl: './totalcontacts.component.html',
  styleUrls: ['./totalcontacts.component.css']
})
export class TotalcontactsComponent implements OnInit {
  
  contacts:Contact[];
  numeroContact:String;

  constructor(
    public dataService:DataService
  ) { }

  ngOnInit(): void {
  }

  calcularContact(){
        
    this.contacts=this.dataService.getContact();
    this.numeroContact=this.contacts.length.toString();
  }

}
